function get_path(types, paths, father_id) {
	if (!father_id) {
		return
	}
	var obj = types.getObj({
		type_id: father_id
	});
	if (obj) {
		paths.push(obj);
		get_path(types, paths, obj.father_id);
	}
}

function get_active(list_type, actives, type_id) {
	var obj = list_type.getObj({
		type_id
	});
	if (obj) {
		obj.active = 'active';
		actives.push(obj);
		get_active(list_type, actives, obj.father_id);
	}
}

/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	var {
		request
	} = ctx;
	var {
		query
	} = request;

	// 分组、分类查询条件
	var _qy = Object.assign({}, query);
	_qy.page = 1;
	delete _qy.size;

	query.page = Number(query.page || 1);
	query.size = Number(query.size || 10);

	var model = {
		list_group: [],
		list: [],
		count: 0,
		group_count: 0,
		nav
	};
	var user = await this.get_state(ctx, db) || {
		user_id: 0,
		gm: 0,
		vip: 0,
		avatar: "",
		nickname: "",
		username: "",
		phone: ""
	};
	model.user = user;

	var type_id = query.types;
	// 获取分类
	var db2 = db.new('source_goods_type', 'type_id');
	db2.size = 0;
	var list_type = await db2.get({
		available: 1,
		show: 1
	}, "`display` asc");

	list_type = list_type.map((o) => {
		o.url = "?" + $.toUrl(Object.assign({}, _qy, {
			types: o.type_id
		}));
		return o
	});

	var actives = [];
	get_active(list_type, actives, type_id);
	actives = actives.reverse();
	model.paths = actives;

	var goods_type = [];
	for (var i = 0; i < actives.length; i++) {
		var o = actives[i];
		var father_id = o.father_id;
		var lt = list_type.get({
			father_id
		});
		if (lt.length) {
			var item;
			if (father_id == '00000') {
				var _q = Object.assign({}, _qy);
				delete _q.types;
				item = {
					name: '全部',
					url: "?" + $.toUrl(_q)
				}
			} else {
				item = {
					name: '全部',
					url: "?" + $.toUrl(Object.assign({}, _qy, {
						types: father_id
					}))
				}
				if (type_id === father_id) {
					item.active = 'active';
				}
			}
			lt.unshift(item);
			goods_type.push(lt);
		}
	}

	var last_id = '00000';
	if (actives.length) {
		var type = actives.pop();
		last_id = type.type_id;
	}
	var lt_type = list_type.get({
		father_id: last_id
	});
	if (lt_type.length) {
		var _q = Object.assign({}, _qy, {
			types: last_id
		});
		if (last_id == '00000') {
			delete _q.types;
		}
		lt_type.unshift({
			name: '全部',
			url: "?" + $.toUrl(_q),
			active: 'active'
		});
		goods_type.push(lt_type);
	}
	model.goods_type = goods_type;
	model.goods_type_tree = list_type.toTree('type_id', '00000');
	if (actives.length) {
		model.obj = actives[0];
	}

	var group_count = 0;
	// 获取资源分组
	var db1 = db.new('source_goods');
	db1.size = 0;
	var qy = Object.assign({}, query);
	qy.method = "count";
	qy.field = "goods_id";
	qy.groupby = "group";
	delete qy.group;
	delete qy.page;
	delete qy.size;
	var group = _qy.group;
	var ret = await this.sql.run(qy, null, db1);
	if (ret.result && ret.result.list) {
		var lt = ret.result.list;
		// 往模型写入相关分组
		model.list_group = lt.map((o) => {
			if (!o.group || o.group === 'null') {
				o.group = '默认';
			}
			group_count += o.count_goods_id;

			var active = "";
			if (group === o.group) {
				active = 'active';
			}
			return {
				name: o.group,
				active,
				url: "?" + $.toUrl(Object.assign({}, _qy, {
					group: o.group
				})),
				count: o.count_goods_id
			};
		});

		model.list_group.unshift({
			name: '全部',
			active: _qy.group ? '' : 'active',
			url: "?" + $.toUrl(Object.assign({}, _qy, {
				group: ''
			})),
			father_id: "00000"
		});

		// 往模型写入相关分组总量
		model.group_count = group_count;
	}

	// 热门资源
	db1.size = 10;
	db1.page = 1;
	var list_hot = await db1.get({}, "`hot` desc");
	model.list_hot = list_hot.map((o) => {
		o.url = "/source/goods_view?goods_id=" + o.goods_id;
		return o;
	});

	// 最新资源
	var list_new = await db1.get({}, "`time_create` desc");
	model.list_new = list_new.map((o) => {
		o.url = "/source/goods_view?goods_id=" + o.goods_id;
		o.time = $.utils.to_time(o.time_create);
		return o;
	});

	// 获取资源
	var q = Object.assign({}, query);
	q.size = 30;
	q.count_ret = 'true';
	if (!q.page) {
		q.page = 1;
	}
	if (type_id) {
		q.types = type_id;
	}
	var ret = await this.sql.run(q, null, db);
	if (ret.result && ret.result.list) {
		var list = ret.result.list;
		if (list.length) {
			var word = query.keyword;
			if (word) {
				var dbt = db.new('cms_tag', 'tag_id');
				var obj = await dbt.getObj({
					name: word,
					url_like: '/source/'
				});
				if (obj) {
					obj.count += 1;
				} else {
					dbt.add({
						name: word,
						count: 1,
						url: '/source/goods_list?page=1&keyword=' + word
					});
				}
			}

			// 获取用户信息
			var users = await $.func.get_user_list(list);

			// 往模型写入资源
			model.list = list.map((o) => {
				var tags = o.tags ? o.tags.trim().split(' ') : [];
				o.tags = tags.map((tag) => {
					return {
						name: tag,
						url: "?" + $.toUrl(Object.assign({}, _qy, {
							keyword: tag
						}))
					};
				});
				var type = (o.types || "").split(",");
				var types = [];
				for (var i = 0; i < goods_type.length; i++) {
					var t = goods_type[i];
					if (type.indexOf(t.type_id) !== -1) {
						types.push(t);
					}
				}
				o.types = types;
				o.time = $.utils.to_time(o.time_create);
				o.url = "/source/goods_view?goods_id=" + o.goods_id;

				var u = users.getObj({
					user_id: o.user_id
				});
				if (u) {
					o.nickname = u.nickname;
					o.avatar = u.avatar;
					o.username = u.username;
				}
				return o;
			});
		}
		// 往模型写入相关资源总量
		model.count = ret.result.count || 0;
	}
	// 获取分页器
	var nav = {};
	var page_count = Math.ceil(model.count / q.size);
	var page = Number(q.page);

	nav.previous = "";
	if (page > 1) {
		q.page = page - 1;
		nav.previous = "?" + $.toUrl(q);
	}

	nav.next = "";
	if (page < page_count) {
		q.page = page + 1;
		nav.next = "?" + $.toUrl(q);
	}
	nav.page_now = page;
	nav.page_size = Number(query.size);
	nav.page_count = page_count;

	delete q.size;
	delete q.page;
	delete q.count_ret;
	var base_url = "?" + $.toUrl(q);
	if (!base_url.endWith('?')) {
		base_url += "&";
	}
	nav.base = base_url;
	model.nav = nav;

	delete _qy.keyword;
	model.search = "?" + $.toUrl(_qy) + "&keyword=";

	// 渲染前进行SEO优化
	await db.tpl.runFunc('seo', ctx.request);
	return db.tpl.view('./source/goods_list.html', model);
};

exports.main = main;