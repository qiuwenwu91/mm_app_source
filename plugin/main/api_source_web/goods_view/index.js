function get_path(types, paths, father_id) {
	if (!father_id) {
		return
	}
	var obj = types.getObj({
		type_id: father_id
	});
	if (obj) {
		paths.push(obj);
		get_path(types, paths, obj.father_id);
	}
}

/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	var {
		request
	} = ctx;
	var {
		query
	} = request;

	var model = {
		obj: {},
		obj_next: {},
		obj_prev: {}
	};
	var user = await this.get_state(ctx, db) || {
		user_id: 0,
		gm: 0,
		vip: 0,
		avatar: "",
		nickname: "",
		username: "",
		phone: ""
	};
	model.user = user;

	// 获取资源
	query.method = "get_obj";
	var ret = await this.sql.run(query, null, db);
	if (ret.result && ret.result.obj) {
		var obj = ret.result.obj;
		// 阅读数加1
		db.set({
			goods_id: obj.goods_id
		}, {
			hot: obj.hot + 1
		});

		// 点赞数
		var db1 = db.new("cms_record");
		var count = await db1.count({
			link_table: 'source_goods',
			link_id: obj.goods_id,
			praise: 1
		});
		obj.praise = count;

		if (user.user_id) {
			var ct = await db1.getObj({
				link_table: 'source_goods',
				user_id: user.user_id,
				link_id: obj.goods_id
			});
			if (ct) {
				// 是否已点赞
				obj.praised = ct.praise;
				// 是否已收藏
				obj.collected = ct.collect;
				// 是否已购买
				obj.buyed = ct.buy;
			}
		}

		obj.tags = obj.tags ? obj.tags.split(',') : [];

		// 获取作者信息
		var dbu = db.new('user_account', 'user_id');
		var u = await dbu.getObj({
			user_id: obj.user_id
		});
		obj.nickname = '(匿名)';
		obj.avatar = '/img/avatar.png';
		if (u) {
			obj.nickname = u.nickname || '(匿名)';
			obj.avatar = u.avatar || '/img/avatar.png';
			obj.qq = u.qq || '';
		}

		model.obj = obj;

		// 获取上一篇资源
		var obj_prev = await db.getObj({
			goods_id_max: obj.goods_id - 1,
			group: obj.group
		}, 'goods_id desc', 'goods_id,title');
		if (!obj_prev) {
			obj_prev = await db.getObj({
				goods_id_max: obj.goods_id - 1
			}, 'goods_id desc', 'goods_id,title');
		}
		model.obj_prev = obj_prev;

		// 获取下一篇资源
		var obj_next = await db.getObj({
			goods_id_min: obj.goods_id + 1,
			group: obj.group
		}, 'goods_id asc', 'goods_id,title');
		if (!obj_next) {
			obj_next = await db.getObj({
				goods_id_min: obj.goods_id + 1
			}, 'goods_id asc', 'goods_id,title');
		}
		model.obj_next = obj_next;
	} else {
		// 使用this.sql.run会在db.ret存结果，因此需要先清理再返回
		db.ret = null;
		return;
	}

	var _qy = Object.assign({}, query);
	var type_id = _qy.type_id || _qy.types;

	if (!type_id) {
		var arr = model.obj.types.split(',');
		if (arr.length) {
			type_id = arr[0];
		}
	}

	// 获取资源分类
	var db2 = db.new('source_goods_type', 'type_id');
	db2.size = 0;
	var list_type = await db2.get({
		available: 1,
		show: 1
	}, "`display` asc");
	var goods_type = list_type.map((o) => {
		o.url = "/source/goods_list?types=" + o.type_id;
		if (type_id === o.type_id) {
			o.active = 'active';
		}
		return o
	});

	var item = {
		name: '全部',
		url: "/source/goods_list?",
		father_id: "00000"
	}
	if (!type_id) {
		item.active = 'active';
	}

	goods_type.unshift(item);

	model.goods_type = goods_type;
	model.goods_type_tree = goods_type.toTree('type_id', '00000');

	model.paths = [];
	if (type_id) {
		// 获取当前分类
		var t = await db2.getObj({
			type_id
		});
		t = Object.assign({}, t);
		// 获取
		if (t) {
			t.url = "/source/goods_list?types=" + t.type_id;
			model.paths.push(t);
			get_path(model.goods_type, model.paths, t.father_id);
			model.paths.reverse()
		}
		model.type = t;
	}

	// 热门资源
	var db1 = db.new('source_goods');
	db1.size = 10;
	db1.page = 1;
	var list_hot = await db1.get({}, "`hot` desc");
	model.list_hot = list_hot.map((o) => {
		o.url = "/source/goods_view?goods_id=" + o.goods_id;
		return o;
	});

	// 获取相关资源
	var qy = {
		page: 1,
		size: 10,
		method: "get",
		orderby: "`time_create` desc",
		field: "title,goods_id,types,time_create,time_update,hot"
	};
	if (obj.group) {
		qy.group = obj.group;
	} else {
		qy.types = obj.types.replaceAll(",", "|");
	}
	var ret = await this.sql.run(qy, null, db);
	if (ret.result) {
		var list_similar = ret.result.list;
		model.list_similar = list_similar.map((o) => {
			o.url = "/source/goods_view?goods_id=" + o.goods_id;
			o.time = $.utils.to_time(o.time_create);
			return o;
		});
	} else {
		model.list_similar = []
	}

	// 渲染前进行SEO优化
	await db.tpl.runFunc('seo', ctx.request);
	return db.tpl.view('./source/goods_view.html', model);
};

exports.main = main;