function get_class(o) {
	var str = o.title.md5();
	var num = Number(str.replace(/[a-zA-Z]+/gi, ""));
	var n = num % 5;
	var cs = "";
	switch (n) {
		case 1:
			cs = "bg_info";
			break;
		case 2:
			cs = "bg_warning";
			break;
		case 3:
			cs = "bg_success";
			break;
		case 4:
			cs = "bg_error";
			break;
		default:
			cs = "bg_primary";
			break;
	}
	return cs;
}

function get_icon(o) {
	if (o.title) {
		return o.title.substring(0, 1);
	}
	return '?'
}

/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	var path = ctx.path;
	var m = {};
	var user = await this.get_state(ctx, db);
	if (!user) {
		user = {
			user_id: 0,
			avatar: "",
			nickname: "",
			username: "",
			phone: ""
		}
	}
	m.user = user;

	var db1 = db.new("sys_nav", "nav_id");
	db1.size = 0;
	var list = await db1.get({
		app: "source",
		available: 1
	});

	for (var i = 0; i < list.length; i++) {
		var o = list[i];
		o.class = get_class(o);
		o.first = get_icon(o);
	}
	m.tabs = list.toTree('nav_id');
	var tpl = db.tpl;
	await tpl.runFunc('seo', ctx.request);
	var bag = tpl.viewBag;
	bag.current_theme = $.globalBag.conf.theme_tpl_nav || $.globalBag.conf.theme_tpl || 'default';
	tpl.current_theme = bag.current_theme;
	return tpl.view(`./source/nav.html`, m);
};

exports.main = main;