/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	var path = ctx.path;
	var m = {};
	var user = await this.get_state(ctx, db);
	if (!user) {
		user = {
			user_id: 0,
			avatar: "",
			nickname: "",
			username: "",
			phone: ""
		}
	}
	m.user = user;
	if (user.user_id) {
		var db0 = db.new('user_account', 'user_id');
		var u = await db0.getObj({
			user_id: user.user_id
		});
		if (u) {
			m.user = Object.assign({}, user, u);
		}
	}
	if (path == '/source/goods_view' || path == '/source/goods_list' || path == '/source/nav') {
		return null;
	}
	await db.tpl.runFunc('seo', ctx.request);
	return db.tpl.view(`.${path}.html`, m);
};

exports.main = main;