var lock_user = {};

/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	var user = db.user;
	// 获取请求数据
	var user_id = user.user_id;

	if (lock_user[user_id]) {
		return $.ret.error(70000, "操作过于频繁，请稍后重试！");
	}
	lock_user[user_id] = true;
	setTimeout(() => {
		lock_user[user_id] = false;
	}, 60000);

	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;

	var {
		coupon_id
	} = body;

	// 获取激活码
	var dbl = Object.assign({}, db);
	dbl.table = "source_activate_code";
	dbl.key = "code_id";
	var code = await dbl.getObj({
		user_id,
		state: 1
	});

	// 校验激活码存在
	if (!code) {
		return $.ret.bl(false, "没有可用的激活码!");
	}

	// 建立数据库操作分支
	var db2 = Object.assign({}, db);

	// 获取优惠券
	db2.table = "source_coupon";
	db2.key = "coupon_id";
	var coupon = await db2.getObj({
		coupon_id
	});

	// 校验优惠券存在
	if (!coupon) {
		return $.ret.bl(false, "优惠券不存在，请选择正确的优惠券!");
	}

	// 校验优惠券状态
	if (coupon.state !== 2) {
		return $.ret.bl(false, "优惠券状态异常，该优惠券已被激活或者过期了!");
	}

	// 判断是否是自己使用
	code.state = 3;
	code.show = 1;
	coupon.state = 3;
	coupon.activate_code = code.activate_code;
	await $.activate_flow(db, 0, user_id, -1, "使用",  "", code.activate_code);
	// 返回结果
	return $.ret.bl(true, "优惠券激活成功！");
};

exports.main = main;