/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	var ret;
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;

	// 操作sql模板
	var {
		key,
		code
	} = query;
	if (!key) {
		
	}
	else {
		db.table = "source_activate_code";
		var obj = await db.getObj({
			activate_code: key
		});
		return $.ret.obj(obj);
	}
	return ret;
};

exports.main = main;