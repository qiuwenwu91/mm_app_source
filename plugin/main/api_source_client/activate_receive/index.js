/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;

	var ret;
	var {
		user_id,
		group
	} = Object.assign({}, query, body);

	var db1 = await db.new("source_activate_code");
	var obj = await db1.getObj({
		user_id: 1,
		state: 1,
		group
	});
	if (!obj) {
		return $.ret.bl(false, "没有了");
	}
	if (user_id == obj.user_id) {
		return $.ret.error(10000, "领取失败！");
	}

	await db1.set({
		code_id: obj.code_id
	}, {
		user_id
	});

	await $.activate_flow(db, obj.user_id, user_id, "转出", obj.activate_code, "被领取激活码");
	await $.activate_flow(db, user_id, obj.user_id, "购买", obj.activate_code, "领取到激活码");

	var now = new Date();
	var by = {
		time_activate: now.toStr('yyyy-MM-dd hh:mm:ss'),
		time_validity: now.addDays(obj.period).toStr('yyyy-MM-dd hh:mm:ss'),
		state: 2
	}
	await db1.set({
		code_id: obj.code_id
	}, by);
	await $.activate_flow(db, user_id, 0, "使用", obj.activate_code, "使用领取的激活码");

	var ret = $.ret.bl("领取成功！");
	ret.result.obj = obj;
	return ret;
};

exports.main = main;