/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;

	var user = db.user;
	var {
		user_id,
		wallet_address
	} = user;

	// 1.获取用户的数量
	var {
		address,
		num
	} = body;

	// 2.查询现有可赠送总量
	db.table = "source_activate_code";
	db.size = 10000;
	var list = await db.get({
		user_id,
		state: 1,
		show: 0
	});

	num = Number(num);
	if (list.length < num) {
		return $.ret.error(30000, "可显示的激活码不足！");
	}

	db.table = "source_activate_code";
	var err;
	for (var i = 0; i < list.length; i++) {
		if (i >= num) {
			break
		}
		var o = list[i];
		var n = await db.set({
			code_id: o.code_id
		}, {
			show: 1
		});
		if (n <= 0) {
			err = db.error;
			break;
		}
	}

	if (!err) {
		return $.ret.bl(true, "显示成功!");
	} else {
		return $.ret.error(10000, "显示失败!原因：" + JSON.stringify(err));
	}
};

exports.main = main;
