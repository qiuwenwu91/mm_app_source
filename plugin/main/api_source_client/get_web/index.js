const Dom = require('mm_html');
const URL = require('url');

// 假设我们有一个URL
const myUrl = 'http://www.example.com:8000/pathname/?search=test';

function getMetadata(url, html) {
	try {
		var dom = new Dom();
		var jq = dom.toJQ(html);
		// 获取标题 title
		var title = jq('title').text();

		// 获取ico favicon
		var favicon = jq('link[rel="shortcut icon"]').attr('href');
		if (!favicon) {
			favicon = jq('link[rel="icon"]').attr('href') || "/favicon.ico";
		}
		
		if (favicon.indexOf("http") == -1) {
			// 解析URL
			const u = URL.parse(url, true);
			var protocol = u.protocol || "http:";
			var host = u.host;
			url = protocol + "//" + host + "/"
			
			if (favicon.indexOf("//") == 0) {
				favicon = "https:" + favicon;
			} else if (url.endWith("/")) {
				if (!favicon.startWith("/")) {
					favicon = url + favicon;
				} else {
					favicon = url + favicon.replace("/", "");
				}
			} else {
				if (favicon.startWith("/")) {
					favicon = url + favicon;
				} else {
					favicon = url + "/" + favicon;
				}
			}
		}

		// 获取关键词 keywords
		var keywords = jq('meta[name="keywords"]').attr('content');

		// 获取描述 description
		var description = jq('meta[name="description"]').attr('content');

		return {
			title,
			keywords,
			description,
			favicon
		};
	} catch (error) {
		console.error('Error fetching website metadata:', error);
		return {};
	}
}

/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;
	var ret;

	// 操作sql模板
	var m = query.method;
	var param = Object.assign({}, query, body);

	var url = param.url;
	if (param.type_id === undefined) {
		var http = new $.Http();
		var res = await http.get(url);
		if (res && res.body) {
			var obj = getMetadata(url, res.body);
			ret = $.ret.obj(obj);
		} else {
			ret = $.ret.error(10000, "无法获取到网页内容！");
		}
	}
	return ret;
};

exports.main = main;