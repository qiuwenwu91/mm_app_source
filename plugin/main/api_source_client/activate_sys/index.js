const Encrypt = require('mm_crypto');
var encrypt = new Encrypt('www.fman.top', 'QAmbG0HojWHg1JVFOhU5ShwwPjKamH0IgkvKENrJr3R', '1234567');

/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;

	var ret;
	if (req.method == "POST") {
		var {
			info,
			key,
			ip,
			domain,
			code,
			method
		} = Object.assign({}, query, body);

		var date, validity;
		var db1 = db.new("source_activate_code", "code_id");
		var db2 = db.new("source_activate_flow", "flow_id");
		if (code) {
			var validity;
			var note = encrypt.decode(code);
			var arr = note.split("\n");
			for (var i = 0; i < arr.length; i++) {
				var str = arr[i];
				if (str.indexOf("=") !== -1) {
					var item = str.split('=');
					var k = item[0].trim();
					var v = item[1].trim();

					if (k == "info") {
						info = v;
					} else if (k == "key") {
						key = v;
					} else if (k == "ip") {
						ip = v;
					} else if (k == "domain") {
						domain = v;
					} else if (k == "date") {
						date = v;
					} else if (k == "validity") {
						validity = v;
					}
				}
			}

			if (method == "check") {
				return $.ret.obj({
					info,
					key,
					ip,
					domain,
					date,
					validity
				});
			}

			// 获取激活码
			var cd = await db1.getObj({
				activate_code: key
			});
			if (!cd) {
				return $.ret.error(10000, "激活失败！原因：激活码不存在!");
			}
			var time_activate = date;
			var time_validity = date.addDays(cd.period).toStr('yyyy-MM-dd hh:mm:ss');
			if (time_validity !== validity) {
				return $.ret.error(10000, "激活失败！原因：激活码解密错误!");
			}
			var now = new Date();
			if (validity < now) {
				return $.ret.error(10000, "激活失败！原因：激活码已过期!");
			}
			// 建立数据库操作分支
			await db1.set({
				code_id: cd.code_id
			}, {
				show: 1,
				state: 3,
				time_activate: date,
				time_validity
			});

			var flow = await db2.getObj({
				activate_code: key
			});
			if (flow) {
				flow.note = note;
			} else {
				// * @param {Object} db 数据库管理器
				// * @param {Number} user_id 用户ID
				// * @param {Number} user_id2 相关用户ID
				// * @param {String} way 转入|购买|被使用|使用|创建|转出|销毁
				// * @param {String} activate_code 激活码
				// * @param {String} note 备注
				// * @param {Number} num 数量
				await $.activate_flow(db, cd.user_id, cd.user_id, "使用", cd.activate_code, note);
			}
			ret = $.ret.bl(true, "激活成功！");
		} else {
			if (!key) {
				return $.ret.error(30001, "激活密钥（key）是必须的");
			}
			if (!info) {
				return $.ret.error(30001, "系统信息（info）是必须的");
			}
			if (!ip && !domain) {
				return $.ret.error(30001, "IP或域名（domain）必须有一个");
			}
			if (!ip) {
				ip = "";
			}
			if (!domain) {
				domain = "";
			}
			// 获取激活码
			var cd = await db1.getObj({
				activate_code: key
			});

			// 校验激活码存在
			if (!cd) {
				return $.ret.bl(false, "激活码不存在，请输入正确的激活码!");
			}

			var now = new Date();

			// 校验激活码状态
			if (cd.state !== 1) {
				var flow = await db2.getObj({
					activate_code: key
				});
				if (flow) {
					var ip_old, domain_old;
					var note = flow.note;
					var arr = note.split("\n");
					for (var i = 0; i < arr.length; i++) {
						var str = arr[i];
						if (str.indexOf("=") !== -1) {
							var item = str.split('=');
							var k = item[0].trim();
							var v = item[1].trim();
							if (k == "ip") {
								ip_old = v;
							} else if (k == "domain") {
								domain_old = v;
							} else if (k == "date") {
								date = v;
							} else if (k == "validity") {
								validity = v;
							}
						}
					}
					if (domain_old && ip) {
						return $.ret.bl(false, "激活码已绑定过域名!");
					}
					if (ip_old && domain) {
						return $.ret.bl(false, "激活码已绑定过IP!");
					}
					if (domain) {
						var domains = domain_old ? domain_old.split(';') : [];
						var ar = domain.split(';');
						for (var i = 0; i < ar.length; i++) {
							var d = ar[i].trim();
							if (domains.indexOf(d) == -1) {
								domains.push(d);
							}
						}
						var len = domains.length;
						if (len > 2) {
							return $.ret.error(10000, "最多绑定1个泛域名和1个子域名，目前已绑定" + domain_old);
						}
						var top_domain_num = 0;
						var sub_domain_num = 0;
						for (var i = 0; i < len; i++) {
							var d = domains[i].trim();
							if (!/^(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]$/i
								.test(d)) {
								ret = $.ret.error(10000, "域名(" + d + ")不正确！");
								break;
							}
							var arr = d.split('.');
							if (arr.length > 2) {
								sub_domain_num += 1;
								if (sub_domain_num > 1) {
									break;
								}
							} else {
								top_domain_num += 1;
								if (top_domain_num > 1) {
									break;
								}
							}
						}
						if (!ret) {
							if (top_domain_num > 1 || sub_domain_num > 1) {
								ret = $.ret.error(10000, "最多绑定1个泛域名和1个子域名，目前已绑定" + domain_old);
							} else {
								domain = domains.join(";");
							}
						}
					} else {
						var ips = ip_old ? ip_old.split(';') : [];
						var arr = ip.split(';');
						for (var i = 0; i < arr.length; i++) {
							var p = arr[i].trim();
							if (ips.indexOf(p) == -1) {
								ips.push(p);
							}
						}
						var len = ips.length;
						if (len > 2) {
							return $.ret.error(10000, "最多只能绑定2个IP地址！，目前已绑定" + ip_old);
						}
						for (var i = 0; i < len; i++) {
							var p = ips[i].trim();
							if (!
								/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
								.test(p)) {
								ret = $.ret.error(10000, "IP地址(" + p + ")不正确！");
								break;
							}
						}
						if (!ret) {
							ip = ips.join(";");
						}
					}

					if (ret) {
						return ret;
					}

					var text =
						`domain=${domain}\nip=${ip}\ndate=${date}\ninfo=${info}\nkey=${key}\nvalidity=${validity}`;
					return $.ret.obj({
						code: encrypt.encode(text)
					});
				} else {
					return $.ret.bl(false, "激活码已被使用!");
				}
			}
			info = info.replace(/\r/g, "").replace(/\n/g, "^");
			var date = now.toStr('yyyy-MM-dd 00:00:00');
			var validity = date.addDays(cd.period).toStr('yyyy-MM-dd hh:mm:ss');
			var text = `domain=${domain}\nip=${ip}\ndate=${date}\ninfo=${info}\nkey=${key}\nvalidity=${validity}`;

			ret = $.ret.obj({
				code: encrypt.encode(text)
			});
		}
	}
	return ret;
};

exports.main = main;