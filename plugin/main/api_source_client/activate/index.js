var lock_user = {};

/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;

	var {
		code_id,
		activate_code,
		ip,
		domain
	} = Object.assign({}, query, body);
	
	var user = await this.get_state(ctx, db);
	// 获取请求数据
	var user_id = user.user_id;

	// 获取激活码
	var db1 = Object.assign({}, db);
	db1.table = "source_activate_code";
	db1.key = "code_id";
	var code = await db1.getObj({
		activate_code
	});

	// 校验激活码存在
	if (!code) {
		return $.ret.bl(false, "激活码不存在，请输入正确的激活码!");
	}

	// 校验激活码状态
	if (code.state !== 1) {
		return $.ret.bl(false, "激活码状态异常，激活码已被使用或者已过期!");
	}

	// 建立数据库操作分支
	var db2 = Object.assign({}, db);

	// 判断是否是自己使用
	code.show = 1;
	code.state = 3;
	code.activate_code = activate_code;
	var note = "";
	if (ip) {
		note += `ip:${ip};`
	}
	if (domain) {
		note += `domain:${domain};`
	}
	
	var now = new Date();
	var time_activate = now.toStr('yyyy-MM-dd hh:mm:ss');
	var time_validity = now.addDays(code.period).toStr('yyyy-MM-dd hh:mm:ss');
	
	if (user_id === code.user_id) {
		await db1.set({
			code_id: code.code_id
		}, {
			state: 3,
			time_activate,
			time_validity
		});
		await $.activate_flow(db, user_id, code.user_id, "使用", code.activate_code, note);
	} else {
		await db1.set({
			code_id: code.code_id
		}, {
			state: 2,
			time_activate,
			time_validity
		});
		await $.activate_flow(db, user_id, code.user_id, "被使用", code.activate_code, note);
		await $.activate_flow(db, user_id, code.user_id, "使用", code.activate_code, note);
	}

	// 返回结果
	return $.ret.bl(true, "激活成功！");
};

exports.main = main;