/**
 * 新建激活码
 * @param {Number} user_id 用户ID
 * @param {Number} stamp 时间戳
 * @param {Number} i 生成数
 * @return {String} 新的激活码
 */
function new_code(user_id, stamp, i) {
	var code = (user_id + "_" + stamp + "_" + i).md5().toLocaleUpperCase();
	return code.substring(0, 5) + "-" + code.substring(5, 10) + "-" + code.substring(10, 15) + "-" + code.substring(15,
		20) + "-" + code.substring(20, 25)
}

/**
 * 生成激活码
 * @param {Object} db 数据库管理器
 * @param {Number} user_id 用户ID
 * @param {Number} num 生成数量
 * @param {Number} creator_id 造物者ID
 * @param {Number} period 有效时长（天）
 * @param {String} codes 代码
 * @param {String} rule 生成规则
 * @return {Boolean} 制造成功返回true，失败返回false
 */
$.gain_activate_code = async function(db, user_id, num, creator_id = 1, period = 0, codes = '', rule = '', group = '') {
	var time = new Date();
	var db1 = db.new("source_activate_code", "code_id");

	var conf = await $.config.conf();
	if (!period) {
		period = conf.activation_period || 365;
	}
	var time_create = time.toStr("yyyy-MM-dd hh:mm:ss");
	var time_end = time.addSeconds(period * 60);
	var time_validity = time_end.toStr("yyyy-MM-dd hh:mm:ss");
	var n = 0;
	var stamp = time.stamp();
	if (codes) {
		var arr = codes.split('\n');
		for (var i = 0; i < arr.length; i++) {
			var activate_code = arr[i];
			var obj = await db1.getObj({
				activate_code
			});
			var bl = 1;
			if (!obj) {
				bl = await db1.add({
					activate_code,
					period,
					time_validity,
					user_id,
					creator_id,
					time_create,
					group
				});
			}
			if (bl <= 0) {
				n = 0;
				break;
			} else {
				n++;
			}
		}
	} else {
		for (var i = 0; i < num; i++) {
			var activate_code = new_code(user_id, stamp, i);
			var bl = await db1.add({
				activate_code,
				period,
				time_validity,
				user_id,
				creator_id,
				time_create,
				group
			});
			if (bl <= 0) {
				n = 0;
				break;
			} else {
				n++;
			}
		}
	}
	if (!n) {
		$.log.error("生成激活失败！", db.error)
	}
	return n
};

/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;
	var ret = "";
	var user = db.user;

	var user_id = user.user_id;
	var content = $.sign(user, body);
	if (!content) {
		return $.ret.error(70000, "非法请求！");
	}
	var arr = content.split("\r\n");
	var address = arr[0].toLocaleLowerCase();
	var amount = Number(arr[1]);

	if (amount.toString().indexOf(".") !== -1) {
		return $.ret.error(30000, "数量必须是整数！");
	}
	return $.ret.bl(true, "购买成功！");
};

exports.main = main;