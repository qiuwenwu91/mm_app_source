/**
 * 创建激活码流水
 * @param {Object} db 数据库管理器
 * @param {Number} user_id 用户ID
 * @param {Number} user_id2 相关用户ID
 * @param {String} way 转入|购买|被使用|使用|创建|转出|销毁
 * @param {String} activate_code 激活码
 * @param {String} note 备注
 * @param {Number} num 数量
 * @return {Boolean} 成功返回true, 失败返回false
 */
$.activate_flow = async function(db, user_id, user_id2 = 0, way = "", activate_code = '', note = "", num = 1) {
	var db1 = Object.assign({}, db);
	db1.table = "user_account";
	var user = await db1.getObj({
		user_id
	});
	if (!user) {
		return false;
	}

	var from_user_id = 0;
	var to_user_id = 0;
	switch (way) {
		case "转入":
			// 从相关账号转入给了自己
			from_user_id = user_id2;
			to_user_id = user_id;
			break;
		case "购买":
			// 从指定账户转到自己的账户
			from_user_id = user_id2;
			to_user_id = user_id;
			break;
		case "被使用":
			// 从自己的账户到指定账户
			from_user_id = user_id;
			to_user_id = user_id2;
			if (num > 0) {
				num = 0 - num;
			}
			break;
		case "使用":
			// 从自己的账户到黑洞账户
			from_user_id = user_id;
			if (num > 0) {
				num = 0 - num;
			}
			break;
		case "创建":
			// 从黑洞账户到自己的账户
			to_user_id = user_id;
			break;
		case "转出":
			// 从自己的账户转入到相关账号
			from_user_id = user_id;
			to_user_id = user_id2;
			if (num > 0) {
				num = 0 - num;
			}
			break;
		case "销毁":
			// 从自己的账户转入到黑洞账号
			from_user_id = user_id;
			if (num > 0) {
				num = 0 - num;
			}
			break;
		default:
			break;
	}

	var db2 = Object.assign({}, db);
	db2.table = "source_activate_code";
	var surplus = await db2.count({
		user_id,
		state: 1
	});

	var db3 = Object.assign({}, db);
	db3.table = "source_activate_flow";
	var bl = await db3.add({
		activate_code,
		from_user_id,
		to_user_id,
		surplus,
		user_id,
		num,
		way,
		note
	});
	return bl > 0
};

/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;

	var {
		user_id,
		num,
		codes,
		period,
		rule,
		group
	} = body;
	var user = await this.get_state(ctx, db);
	var from_user_id = user.user_id;
	if (!codes && !num) {
		return $.ret.error(30001, "创建的数量不能小于1");
	}
	var n = await $.gain_activate_code(db, user_id, num, from_user_id, period, codes, rule, group);
	if (n) {
		await $.activate_flow(db, user_id, 0, "创建", "", `【${user.nickname}】创建激活码x` + n, n);
		return $.ret.bl(true, "创建激活码成功");
	} else {
		return $.ret.bl(false, "创建激活失败");
	}
};

exports.main = main;