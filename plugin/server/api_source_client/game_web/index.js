var log = {};

/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;

	// 操作sql模板
	var m = query.method;
	var ip = ctx.ip;
	if (m === "add") {
		var o = log[ip];
		if (o) {
			var now = new Date();
			if (now < o.time.addSeconds(7200)) {
				return $.ret.error(70001, "已发布过游戏！");
			}
		} else {
			if (!body.ip) {
				body.ip = await $.utils.ip(body.url);
			}
		}
		body.available = 0;
	} else if (m === "demo") {
		var star = 5;
		var hot = 100000;
		var db1 = db.new("source_game_web");
		var list = "./data.json".loadJson(__dirname);
		for (var i = 0; i < list.length; i++) {
			var o = list[i];
			var ip = await $.utils.ip(o.url);
			if (ip) {
				await db1.add({
					star: star.rand(),
					hot: hot.rand(),
					image: o.image,
					name: o.title,
					title: o.description,
					url: o.url,
					ip
				});
			}
		}
		return $.ret.bl(true, "示例导入完成");
	} else if (m === "check") {
		query.method = "get";

	}
	var ret = await this.sql.run(query, body, db);
	if (m === "add" && ret.result.bl) {
		var o = log[ip];
		var now = new Date();
		if (o) {
			o.time = now;
		} else {
			log[ip] = {
				time: now
			}
		}
	} else if (m === "check" && ret.result) {
		var user = await this.get_state(ctx, db);
		if (!user || !user.gm) {
			return $.ret.error(70001, "没有访问权限！");
		}
		var http = new $.Http();
		var list = ret.result.list;
		var db2 = db.new("source_game_web", "web_id");
		for (var i = 0; i < list.length; i++) {
			var o = list[i];
			var body = "";
			if (o.url) {
				var res = await http.get(o.url);
				body = res.body;
			}
			if (!body) {
				db2.set({
					web_id: o.web_id
				}, {
					state: 4,
					available: 0
				});
			}
		}
		ret = $.ret.bl(true, "检测完成！");
	}
	return ret;
};

exports.main = main;